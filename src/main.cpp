// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// A basic main, showing the  supplied timer
// and trace output functions.
//
// ----------------------------------------------------------------------------


#include <stdio.h>

#include "diag/Trace.h"
#include "Timer.h"

int main()
{
  Timer sys_tick;
  sys_tick.start();

  while(true)
  {
    trace_printf("Tick...\n");
    Timer::sleep(1000);
  }
}


// ----------------------------------------------------------------------------
